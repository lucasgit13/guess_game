use rand::Rng;
use std::io;

extern crate rand; // Inporting external crate

fn main() {
    let begin_range = 1;
    let end_range = 100;
    // let correct: u8 = random();
    let correct = rand::thread_rng().gen_range(begin_range..=end_range);
    let out_of_guesses = 3;
    let _guess_count = 1;

    println!("Wellcome to no guess game!");
    println!("Input a number between {} to {}", begin_range, (end_range));
    println!("The correct value is: {}\n", correct);

    for _guess_count in 0..out_of_guesses {
        let guess = get_guess();
        if handle_guess(guess, correct) {
            break;
        }
        println!("Guess ramainder: {}", (out_of_guesses - 1) - _guess_count);
    }
}

//-

fn get_guess() -> u8 {
    loop {
        println!("Input guess:");
        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Coudnt read from stdin");
        match guess.trim().parse::<u8>() {
            // Trim the input to remove spaces
            Ok(v) => return v,
            Err(e) => println!("Coundnt understand the input {}", e),
        }
    }
}

//-

fn handle_guess(guess: u8, correct: u8) -> bool {
    if guess < correct {
        println!("Too low");
        return false;
    } else if guess > correct {
        println!("Too high");
        return false;
    } else {
        println!("You got it!");
        return true;
    }
}
